﻿// Write your JavaScript code.
function ajaxLoad(Type, Url, Data, Success, Done) {
  $.ajax({
    type: Type,
    contentType: 'application/json; charset=utf-8',
    //dataType: 'json',
    url: Url,
    data: Data,
    success: Success,
    error: function (data) {
      console.log(data.responseText);

    }
  }).done(Done);
}

function ShowError(elemnt, message) {
  elemnt.append('<div class="alert alert-danger" role="alert">' + message + '</div>');
}

function DisplayError(id, error) {
  var html = "<div class='alert alert-dismissible alert-danger col-sm-12 col-lg-offset-2 col-lg-8'>" +
    "<button type='button' class='close' data-dismiss='alert'>&times;</button>" +
    "<strong>Error! </strong>" + error +
    "</div>";

  $("#" + id).append(html);
}

function ClearError(elemnt) {
  elemnt.html('');
}

function EsperarSwal(title, message) {
  swal({
    title: title,
    text: message,
    timer: 0,
    onOpen: function () {
      swal.showLoading()
    },
    allowOutsideClick: false
  }).then(function (dismiss) {
    if (dismiss === 'esc' || dismiss == 'close') {
      swal('Atencion',
        'Recibira una notificacion del estatus del proceso mas tarde o verificar si el proceso se hizo correctamente manualmente',
        'info')
    }
  });
}

function FinalizarEspera() {
  swal.close();
}

$(document).on('keyup', '.with-format-natural',
  function (evt) {
    var selection = window.getSelection().toString();
    if (selection !== '') {
      return;
    }
    if ($.inArray(evt.keyCode, [38, 40, 37, 39]) !== -1) {
      return;
    }
    var input = $(this).val().replace(/[\D\s\._\-]+/g, "");

    input = input ? parseInt(input, 10) : 0;

    $(this).val(function () {
      return (input === 0) ? "" : input;
    });
  }
);

$(document).on('keyup', '.with-format-decimal',
  function (evt) {
    var selection = window.getSelection().toString();
    if (selection !== '') {
      return;
    }
    if ($.inArray(evt.keyCode, [38, 40, 37, 39]) !== -1) {
      return;
    }
    var input = $(this).val().replace(/[^\d+(\.\d+)*]+/g, "");

    input = input ? input : 0;

    $(this).val(function () {
      return (input === 0) ? "" : input;
    });
  }
);


//==========================================================================
$('.sidebar .sidebar-menu li a').on('click', function () {
  const $this = $(this);

  if ($this.parent().hasClass('open')) {
    $this
      .parent()
      .children('.dropdown-menu')
      .slideUp(200, () => {
        $this.parent().removeClass('open');
      });
  } else {
    $this
      .parent()
      .parent()
      .children('li.open')
      .children('.dropdown-menu')
      .slideUp(200);

    $this
      .parent()
      .parent()
      .children('li.open')
      .children('a')
      .removeClass('open');

    $this
      .parent()
      .parent()
      .children('li.open')
      .removeClass('open');

    $this
      .parent()
      .children('.dropdown-menu')
      .slideDown(200, () => {
        $this.parent().addClass('open');
      });
  }
});

// Sidebar Activity Class
const sidebarLinks = $('.sidebar').find('.sidebar-link');

sidebarLinks
  .each((index, el) => {
    $(el).removeClass('active');
  })
  .filter(function () {
    const href = $(this).attr('href');
    const pattern = href[0] === '/' ? href.substr(1) : href;
    return pattern === (window.location.pathname).substr(1);
  })
  .addClass('active');

// ٍSidebar Toggle
$('.sidebar-toggle').on('click', e => {
  $('.app').toggleClass('is-collapsed');
  e.preventDefault();
});

$(document).on('keyup', '.with-format-price',
    function (evt) {
        var selection = window.getSelection().toString();
        if (selection !== '') {
            return;
        }
        if ($.inArray(evt.keyCode, [38, 40, 37, 39]) !== -1) {
            return;
        }
        var input = $(this).val().replace(/[\D\s\._\-]+/g, "");

        input = input ? parseInt(input, 10) : 0;

        $(this).val(function () {
            return (input === 0) ? "0" : input.toLocaleString("es-MX");
        });
    }
);

/**
 * Wait untill sidebar fully toggled (animated in/out)
 * then trigger window resize event in order to recalculate
 * masonry layout widths and gutters.
 */
$('#sidebar-toggle').click(e => {
  e.preventDefault();
  setTimeout(() => {
    //   window.dispatchEvent(window.EVENT);
  }, 300);
});

//Counter
$(function () {
  $('[data-decrease]').click(decrease);
  $('[data-increase]').click(increase);
  $('[data-value]').change(valueChange);
});

function decrease() {
  var value = $(this).parent().find('[data-value]').val();
  if (value >= 1) {
    value--;
    $(this).parent().find('[data-value]').val(value);
  }
}

function increase() {
  var value = $(this).parent().find('[data-value]').val();
  if (value < 100) {
    value++;
    $(this).parent().find('[data-value]').val(value);
  }
}

function valueChange() {
  var value = $(this).val();
  if (value == undefined || isNaN(value) == true || value <= 0) {
    $(this).val(1);
  } else if (value >= 101) {
    $(this).val(100);
  }
}