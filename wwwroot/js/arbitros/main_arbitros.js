var bundle = {
    page: 0,
    take: 20
}


$(document).ready(function () {
    GetPartidos();
});

function GetPartidos() {
    ajaxLoad("POST", "/Arbitros/GetPartidos/", 0, function (data) {
        var response = JSON.parse(data);

        $("#result").html(response.html);
    });
}