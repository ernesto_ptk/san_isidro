
var check_locales = 0;
var check_visitantes = 0;
var take_local = false;
var take_visita = false;

var notification_can_save_asistencia = false;

$(document).on('click', ".card-player", function () {
    var element = $(this).hasClass('selected');

    if (element) {
        $(this).removeClass('selected');
        $(this).find("input[type='checkbox']").prop("checked", false);
    }
    else {
        $(this).addClass('selected');
        $(this).find("input[type='checkbox']").prop("checked", true);
    }

    $('.card-player.selected input[type="checkbox"]').prop("checked", true);
});

$(document).on('click', '#locales .card-player', function () {
    var checked = $("#locales input:checked").length;
    var count = $("#locales input").length;

    check_locales = checked;

    $("#countLocal").html('( ' + checked + ' / ' + count + ' ) ');

    SePuedeJugar();
});

$(document).on('click', '#visitantes .card-player', function () {
    var checked = $("#visitantes input:checked").length;
    var count = $("#visitantes input").length;

    check_visitantes = checked;

    $("#countVisita").html('( ' + checked + ' / ' + count + ' ) ');

    SePuedeJugar();
});

function SePuedeJugar() {
    if (check_locales >= 6 && check_visitantes >= 6 && !notification_can_save_asistencia) {
        swal('Éxito', "Ambos equipos tienen el mínimo para jugar el partido", 'success');
        $("#guardarAsistencia").prop('disabled', false);
        notification_can_save_asistencia = true;
    } else {
        $("#guardarAsistencia").prop('disabled', true);
    }
}

function DefinirJugadores() {

}

var videoLocal;

$(document).on('click', "#fotoLocal", function () {
    // Grab elements, create settings, etc.
    videoLocal = document.getElementById('videoLocal');

    // Get access to the camera!
    if (navigator.mediaDevices && navigator.mediaDevices.getUserMedia) {
        // Not adding `{ audio: true }` since we only want video now
        navigator.mediaDevices.getUserMedia({ video: { facingMode: { exact: "environment" } } }).then(function (stream) {
            //video.src = window.URL.createObjectURL(stream);
            $("#zonaLocal").removeClass('d-none');
            videoLocal.srcObject = stream;
            videoLocal.play();

        });
    } else if (navigator.getUserMedia) { // Standard
        navigator.getUserMedia({ video: { facingMode: { exact: "environment" } } }, function (stream) {
            $("#zonaLocal").removeClass('d-none');
            videoLocal.src = stream;
            videoLocal.play();
        }, errBack);
    } else if (navigator.webkitGetUserMedia) { // WebKit-prefixed
        navigator.webkitGetUserMedia({ video: { facingMode: { exact: "environment" } } }, function (stream) {
            $("#zonaLocal").removeClass('d-none');
            videoLocal.src = window.webkitURL.createObjectURL(stream);
            videoLocal.play();
        }, errBack);
    } else if (navigator.mozGetUserMedia) { // Mozilla-prefixed
        navigator.mozGetUserMedia({ video: { facingMode: { exact: "environment" } } }, function (stream) {
            $("#zonaLocal").removeClass('d-none');
            videoLocal.srcObject = stream;
            videoLocal.play();
        }, errBack);
    }
});

$(document).on('click', "#snapLocal", function () {
    var canvas = document.getElementById('canvasLocal');
    var context = canvas.getContext('2d');
    var video = document.getElementById('videoLocal');

    context.drawImage(video, 0, 0, 640, 480);

    videoLocal.pause();
    videoLocal.src = '';

    $("#zonaLocal").find('.d-flex').addClass('d-none');
    $("#zonaLocal").find('.d-none').removeClass('d-flex');
    $("#zonaLocal").find('canvas').removeClass('d-none');

    take_local = true;

    CheckFotos();
});

$(document).on('click', "#repeatLocal", function () {
    $("#zonaLocal").find('.d-none').addClass('d-flex');
    $("#zonaLocal").find('.d-flex').removeClass('d-none');
    $("#zonaLocal").find('canvas').addClass('d-none');

    take_local = false;

    CheckFotos();
});

var videoVisita;

$(document).on('click', "#fotoVisita", function () {
    // Grab elements, create settings, etc.
    videoVisita = document.getElementById('videoVisita');

    // Get access to the camera!
    if (navigator.mediaDevices && navigator.mediaDevices.getUserMedia) {
        // Not adding `{ audio: true }` since we only want video now
        navigator.mediaDevices.getUserMedia({ video: { facingMode: { exact: "environment" } } }).then(function (stream) {
            //video.src = window.URL.createObjectURL(stream);
            $("#zonaVisita").removeClass('d-none');
            videoVisita.srcObject = stream;
            videoVisita.play();
        });
    } else if (navigator.getUserMedia) { // Standard
        navigator.getUserMedia({ video: { facingMode: { exact: "environment" } } }, function (stream) {
            $("#zonaVisita").removeClass('d-none');
            videoVisita.src = stream;
            videoVisita.play();
        }, errBack);
    } else if (navigator.webkitGetUserMedia) { // WebKit-prefixed
        navigator.webkitGetUserMedia({ video: { facingMode: { exact: "environment" } } }, function (stream) {
            $("#zonaVisita").removeClass('d-none');
            videoVisita.src = window.webkitURL.createObjectURL(stream);
            videoVisita.play();
        }, errBack);
    } else if (navigator.mozGetUserMedia) { // Mozilla-prefixed
        navigator.mozGetUserMedia({ video: { facingMode: { exact: "environment" } } }, function (stream) {
            $("#zonaVisita").removeClass('d-none');
            videoVisita.srcObject = stream;
            videoVisita.play();
        }, errBack);
    }
});

$(document).on('click', "#snapVisita", function () {
    var canvas = document.getElementById('canvasVisita');
    var context = canvas.getContext('2d');
    var video = document.getElementById('videoVisita');

    context.drawImage(video, 0, 0, 640, 480);

    videoVisita.pause();
    videoVisita.src = '';

    $("#zonaVisita").find('.d-flex').addClass('d-none');
    $("#zonaVisita").find('.d-none').removeClass('d-flex');
    $("#zonaVisita").find('canvas').removeClass('d-none');

    take_visita = true;

    CheckFotos();
});

$(document).on('click', "#repeatVisita", function () {
    $("#zonaVisita").find('.d-none').addClass('d-flex');
    $("#zonaVisita").find('.d-flex').removeClass('d-none');
    $("#zonaVisita").find('canvas').addClass('d-none');

    take_visita = false;

    CheckFotos();
});

function CheckFotos() {
    if (take_visita && take_local) {
        swal('Éxito', "Ya hay evidencia de ambos equipos, es posible guardar la evidencia en este momento", "success")
        $("#saveEvidencia").prop('disabled', false);;
    } else {
        $("#saveEvidencia").prop('disabled', true);
    }
}


$(document).on("click", "#saveEvidencia", function () {
    var img_local = document.getElementById('canvasLocal').toDataURL();
    var img_visita = document.getElementById('canvasVisita').toDataURL();
    var IdPartido = $("#IdPartido").html();

    ajaxLoad("POST", "/Arbitros/RegistrarEvidenciaPartido/", JSON.stringify({
        IdPartido: IdPartido,
        ImgLocal: img_local,
        ImgVisita: img_visita
    }), function (data) {
        var response = JSON.parse(data);
        if (response.result) {
            swal("Exito", "Se guardo la Evidencia de los Equipos Correctamente", 'success').then(function () {
                $("#zonaLocal").addClass("d-none");
                $("#zonaVisita").addClass("d-none");
            });
        } else {
            swal("Error", response.message, 'info');
        }
    });

});