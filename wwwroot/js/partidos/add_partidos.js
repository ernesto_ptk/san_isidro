var contextPartido = {
    IdCategoria: 0,
    IdLocal: 0,
    IdVisitante: 0,
    Fecha: "",
    Horario: ""
};

$(document).on('click', "#new-partido", function (evt) {
    evt.preventDefault();
    ajaxLoad("GET", "/Partidos/GetModalRegistrar/", 0, function (data) {
        var response = JSON.parse(data);
        $("#modalTitle").text("Registrar Partido");
        $("#modalBody").html(response.html);
        $("#modal").children().css("min-width", "80%");
        $("#modal").appendTo("body").modal({
            backdrop: 'static'
        }).modal("show");

        InitRegisterPartido();

    });
});

function InitRegisterPartido() {
    $("#select-categoria").select2({
        language: "es",
        placeholder: 'Elegir Categoria',
        width: "100%",
        liveSearch: true,
        allowClear: true,
    });
    $('#select-categoria').val(null).trigger("change");

    $("#select-equipo-local").select2({
        language: "es",
        placeholder: 'Elegir Equipo Local',
        width: "100%",
        liveSearch: true,
        allowClear: true,
    });
    $('#select-equipo-local').val(null).trigger("change");

    $("#select-equipo-visitante").select2({
        language: "es",
        placeholder: 'Elegir Equipo Visitante',
        width: "100%",
        liveSearch: true,
        allowClear: true,
    });
    $('#select-equipo-visitante').val(null).trigger("change");

    $('#hora-partido').timepicki();

    $('#fecha-partido').datepicker({
        language: 'es',
        autoClose: true
    });
}

$(document).on("change.select2", "#select-categoria", function () {
    var val = $(this).val();
    if (!val) {
        return;
    } else {
        contextPartido.IdCategoria = val;
        LoadEquipos(contextPartido.IdCategoria, contextPartido.IdLocal, $("#select-equipo-local"));
    }
});

$(document).on("change.select2", "#select-equipo-local", function () {
    var val = $(this).val();
    if (!val) {
        contextPartido.IdLocal = 0;
        contextPartido.IdVisitante = 0;
        $("#select-equipo-visitante").html('');
        return;
    } else {
        contextPartido.IdLocal = val;
        LoadEquipos(contextPartido.IdCategoria, contextPartido.IdLocal, $("#select-equipo-visitante"));
    }
});

$(document).on("change.select2", "#select-equipo-visitante", function () {
    var val = $(this).val();
    if (!val) {
        contextPartido.IdVisitante = 0;
        return;
    } else {
        contextPartido.IdVisitante = val;
    }
});

function LoadEquipos(IdCategoria, IdEquipoLocal, Component) {
    ajaxLoad("POST", "/Partidos/GetEquiposByCategoriaAndWithOut/", JSON.stringify({ IdCategoria, IdEquipoLocal }), function (data) {
        var response = JSON.parse(data);

        Component.html('');
        if (response.result.length != 0) {
            response.result.forEach(function (v, i) {
                Component.append('<option value="' + v.Id + '">' + v.NombreEquipo + '</option>');
            });
        }
        Component.val(null).trigger('change');

    });
}

function CapturePartido() {
    var component = $("#error-container");
    ClearError(component);

    var fecha = $("#fecha-partido").val();
    var hora = $("#hora-partido").val();

    if (contextPartido.IdCategoria == 0) {
        ShowError(component, "Es necesario Definir la Categoria");
    }
    if (contextPartido.IdLocal == 0) {
        ShowError(component, "Es necesario Definir el Equipo Local");
    }
    if (contextPartido.IdVisitante == 0) {
        ShowError(component, "Es necesario Definir el Equipo Visitante");
    }
    if (fecha == "") {
        ShowError(component, "Es necesario Definir la Fecha del Partido.");
    }
    if (hora == "") {
        ShowError(component, "Es necesario Definir la Hora del Partido.");
    }
    if (component.children().length > 0) {
        return null;
    } else {
        contextPartido.Fecha = fecha;
        contextPartido.Horario = hora;
        return true;
    }
}

$(document).on("click", "#add-partido", function (evt) {
    evt.preventDefault();
    var result = CapturePartido();
    if (result == null) {
        return;
    }
    console.log("Pase");
    RegistrarPartido();
});

function RegistrarPartido() {
    ajaxLoad("POST", "/Partidos/RegistrarEquipo/", JSON.stringify(contextPartido), function (data) {
        var response = JSON.parse(data);
        console.log(response);
        if (response.result) {
            $("#modal").modal("hide");
            swal('Exito',
                response.message,
                'success').then(function (result) {
                    GetPartidos();
                });
        } else {
            swal('Advertencia',
                response.message,
                'error');
        }
    });
}