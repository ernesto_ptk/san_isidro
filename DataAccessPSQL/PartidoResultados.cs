﻿using System;
using System.Collections.Generic;

namespace SAN_ISIDRO.DataAccessPSQL
{
    public partial class PartidoResultados
    {
        public int Id { get; set; }
        public int IdPartido { get; set; }
        public int PuntosLocal { get; set; }
        public int PuntosVisitante { get; set; }
        public string Set { get; set; }
        public DateTime FechaCreacion { get; set; }

        public Partido IdPartidoNavigation { get; set; }
    }
}
