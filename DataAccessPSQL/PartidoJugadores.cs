﻿using System;
using System.Collections.Generic;

namespace SAN_ISIDRO.DataAccessPSQL
{
    public partial class PartidoJugadores
    {
        public int Id { get; set; }
        public int IdPartido { get; set; }
        public int IdJugador { get; set; }
        public DateTime FechaCreacion { get; set; }

        public Jugador IdJugadorNavigation { get; set; }
        public Partido IdPartidoNavigation { get; set; }
    }
}
