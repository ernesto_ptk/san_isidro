﻿using System;
using System.Collections.Generic;

namespace SAN_ISIDRO.DataAccessPSQL
{
    public partial class Lugar
    {
        public Lugar()
        {
            Partido = new HashSet<Partido>();
        }

        public int Id { get; set; }
        public string Nombre { get; set; }

        public ICollection<Partido> Partido { get; set; }
    }
}
