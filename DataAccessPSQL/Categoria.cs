﻿using System;
using System.Collections.Generic;

namespace SAN_ISIDRO.DataAccessPSQL
{
    public partial class Categoria
    {
        public Categoria()
        {
            Equipo = new HashSet<Equipo>();
        }

        public int Id { get; set; }
        public string Nombre { get; set; }
        public DateTime FechaCreacion { get; set; }

        public ICollection<Equipo> Equipo { get; set; }
    }
}
