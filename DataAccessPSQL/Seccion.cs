﻿using System;
using System.Collections.Generic;

namespace SAN_ISIDRO.DataAccessPSQL
{
    public partial class Seccion
    {
        public Seccion()
        {
            AspNetRoles = new HashSet<AspNetRoles>();
        }

        public int Id { get; set; }
        public string Nombre { get; set; }

        public ICollection<AspNetRoles> AspNetRoles { get; set; }
    }
}
