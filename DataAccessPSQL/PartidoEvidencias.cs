﻿using System;
using System.Collections.Generic;

namespace SAN_ISIDRO.DataAccessPSQL
{
    public partial class PartidoEvidencias
    {
        public int Id { get; set; }
        public int IdPartido { get; set; }
        public string UrlLocal { get; set; }
        public string UrlVisita { get; set; }
        public string IdUsuarioCreador { get; set; }
        public DateTime FechaCreacion { get; set; }

        public Partido IdPartidoNavigation { get; set; }
        public AspNetUsers IdUsuarioCreadorNavigation { get; set; }
    }
}
