﻿using System;
using System.Collections.Generic;

namespace SAN_ISIDRO.DataAccessPSQL
{
    public partial class Partido
    {
        public Partido()
        {
            PartidoEvidencias = new HashSet<PartidoEvidencias>();
            PartidoJugadores = new HashSet<PartidoJugadores>();
            PartidoResultados = new HashSet<PartidoResultados>();
        }

        public int Id { get; set; }
        public int IdEquipoLocal { get; set; }
        public int IdEquipoVisitante { get; set; }
        public DateTime FechaPartido { get; set; }
        public int IdLugar { get; set; }
        public string IdArbitro { get; set; }
        public DateTime FechaCreacion { get; set; }
        public string IdUsuarioCreador { get; set; }

        public AspNetUsers IdArbitroNavigation { get; set; }
        public Equipo IdEquipoLocalNavigation { get; set; }
        public Equipo IdEquipoVisitanteNavigation { get; set; }
        public Lugar IdLugarNavigation { get; set; }
        public AspNetUsers IdUsuarioCreadorNavigation { get; set; }
        public ICollection<PartidoEvidencias> PartidoEvidencias { get; set; }
        public ICollection<PartidoJugadores> PartidoJugadores { get; set; }
        public ICollection<PartidoResultados> PartidoResultados { get; set; }
    }
}
