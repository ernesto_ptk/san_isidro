﻿using System;
using System.Collections.Generic;

namespace SAN_ISIDRO.DataAccessPSQL
{
    public partial class Jugador
    {
        public Jugador()
        {
            PartidoJugadores = new HashSet<PartidoJugadores>();
        }

        public int Id { get; set; }
        public string Nombre { get; set; }
        public string NombreNormalizado { get; set; }
        public bool IsSocio { get; set; }
        public string Serie { get; set; }
        public string ImagenBase64 { get; set; }
        public DateTime FechaCreacion { get; set; }
        public int IdEquipo { get; set; }
        public string Url { get; set; }

        public Equipo IdEquipoNavigation { get; set; }
        public ICollection<PartidoJugadores> PartidoJugadores { get; set; }
    }
}
