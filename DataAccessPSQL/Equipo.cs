﻿using System;
using System.Collections.Generic;

namespace SAN_ISIDRO.DataAccessPSQL
{
    public partial class Equipo
    {
        public Equipo()
        {
            Jugador = new HashSet<Jugador>();
            PartidoIdEquipoLocalNavigation = new HashSet<Partido>();
            PartidoIdEquipoVisitanteNavigation = new HashSet<Partido>();
        }

        public int Id { get; set; }
        public string Nombre { get; set; }
        public string Representante { get; set; }
        public string RepresentanteOpcional { get; set; }
        public string Celular { get; set; }
        public string CelularNormalizado { get; set; }
        public string Correo { get; set; }
        public string CorreoNormalizado { get; set; }
        public DateTime FechaCreacion { get; set; }
        public string RepresentanteNormalizado { get; set; }
        public string RepresentanteOpcionalNormalizado { get; set; }
        public int IdCategoria { get; set; }
        public string NombreNomalizado { get; set; }
        public string Telefono { get; set; }
        public string TelefonoNormalizado { get; set; }
        public string IdUsuario { get; set; }

        public Categoria IdCategoriaNavigation { get; set; }
        public AspNetUsers IdUsuarioNavigation { get; set; }
        public ICollection<Jugador> Jugador { get; set; }
        public ICollection<Partido> PartidoIdEquipoLocalNavigation { get; set; }
        public ICollection<Partido> PartidoIdEquipoVisitanteNavigation { get; set; }
    }
}
