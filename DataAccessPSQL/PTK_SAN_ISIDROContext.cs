﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace SAN_ISIDRO.DataAccessPSQL
{
    public partial class PTK_SAN_ISIDROContext : DbContext
    {
        public PTK_SAN_ISIDROContext()
        {
        }

        public PTK_SAN_ISIDROContext(DbContextOptions<PTK_SAN_ISIDROContext> options)
            : base(options)
        {
        }

        public virtual DbSet<AspNetRoleClaims> AspNetRoleClaims { get; set; }
        public virtual DbSet<AspNetRoles> AspNetRoles { get; set; }
        public virtual DbSet<AspNetUserClaims> AspNetUserClaims { get; set; }
        public virtual DbSet<AspNetUserLogins> AspNetUserLogins { get; set; }
        public virtual DbSet<AspNetUserRoles> AspNetUserRoles { get; set; }
        public virtual DbSet<AspNetUsers> AspNetUsers { get; set; }
        public virtual DbSet<AspNetUserTokens> AspNetUserTokens { get; set; }
        public virtual DbSet<Categoria> Categoria { get; set; }
        public virtual DbSet<Equipo> Equipo { get; set; }
        public virtual DbSet<Jugador> Jugador { get; set; }
        public virtual DbSet<Lugar> Lugar { get; set; }
        public virtual DbSet<Partido> Partido { get; set; }
        public virtual DbSet<PartidoEvidencias> PartidoEvidencias { get; set; }
        public virtual DbSet<PartidoJugadores> PartidoJugadores { get; set; }
        public virtual DbSet<PartidoResultados> PartidoResultados { get; set; }
        public virtual DbSet<Seccion> Seccion { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
                optionsBuilder.UseNpgsql("Server=localhost;Port=5432;Database=PTK_SAN_ISIDRO;User Id=ptk_client;Password=Webo1330;");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<AspNetRoleClaims>(entity =>
            {
                entity.HasIndex(e => e.RoleId);

                entity.Property(e => e.RoleId).IsRequired();

                entity.HasOne(d => d.Role)
                    .WithMany(p => p.AspNetRoleClaims)
                    .HasForeignKey(d => d.RoleId);
            });

            modelBuilder.Entity<AspNetRoles>(entity =>
            {
                entity.HasIndex(e => e.NormalizedName)
                    .HasName("RoleNameIndex")
                    .IsUnique();

                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.IdSeccion).HasColumnName("Id_Seccion");

                entity.Property(e => e.Name).HasMaxLength(256);

                entity.Property(e => e.NormalizedName).HasMaxLength(256);

                entity.HasOne(d => d.IdSeccionNavigation)
                    .WithMany(p => p.AspNetRoles)
                    .HasForeignKey(d => d.IdSeccion)
                    .HasConstraintName("FK_IdSeccion_Seccion");
            });

            modelBuilder.Entity<AspNetUserClaims>(entity =>
            {
                entity.HasIndex(e => e.UserId);

                entity.Property(e => e.UserId).IsRequired();

                entity.HasOne(d => d.User)
                    .WithMany(p => p.AspNetUserClaims)
                    .HasForeignKey(d => d.UserId);
            });

            modelBuilder.Entity<AspNetUserLogins>(entity =>
            {
                entity.HasKey(e => new { e.LoginProvider, e.ProviderKey });

                entity.HasIndex(e => e.UserId);

                entity.Property(e => e.LoginProvider).HasMaxLength(128);

                entity.Property(e => e.ProviderKey).HasMaxLength(128);

                entity.Property(e => e.UserId).IsRequired();

                entity.HasOne(d => d.User)
                    .WithMany(p => p.AspNetUserLogins)
                    .HasForeignKey(d => d.UserId);
            });

            modelBuilder.Entity<AspNetUserRoles>(entity =>
            {
                entity.HasKey(e => new { e.UserId, e.RoleId });

                entity.HasIndex(e => e.RoleId);

                entity.HasOne(d => d.Role)
                    .WithMany(p => p.AspNetUserRoles)
                    .HasForeignKey(d => d.RoleId);

                entity.HasOne(d => d.User)
                    .WithMany(p => p.AspNetUserRoles)
                    .HasForeignKey(d => d.UserId);
            });

            modelBuilder.Entity<AspNetUsers>(entity =>
            {
                entity.HasIndex(e => e.NormalizedEmail)
                    .HasName("EmailIndex");

                entity.HasIndex(e => e.NormalizedUserName)
                    .HasName("UserNameIndex")
                    .IsUnique();

                entity.Property(e => e.Id).ValueGeneratedNever();

                entity.Property(e => e.Email).HasMaxLength(256);

                entity.Property(e => e.LockoutEnd).HasColumnType("timestamp with time zone");

                entity.Property(e => e.NormalizedEmail).HasMaxLength(256);

                entity.Property(e => e.NormalizedUserName).HasMaxLength(256);

                entity.Property(e => e.UserName).HasMaxLength(256);
            });

            modelBuilder.Entity<AspNetUserTokens>(entity =>
            {
                entity.HasKey(e => new { e.UserId, e.LoginProvider, e.Name });

                entity.Property(e => e.LoginProvider).HasMaxLength(128);

                entity.Property(e => e.Name).HasMaxLength(128);

                entity.HasOne(d => d.User)
                    .WithMany(p => p.AspNetUserTokens)
                    .HasForeignKey(d => d.UserId);
            });

            modelBuilder.Entity<Categoria>(entity =>
            {
                entity.HasKey(e => new { e.Id, e.Nombre });

                entity.HasIndex(e => e.Id)
                    .HasName("Id_Unique")
                    .IsUnique();

                entity.Property(e => e.Id).ValueGeneratedOnAdd();

                entity.Property(e => e.FechaCreacion).HasColumnName("Fecha_Creacion");
            });

            modelBuilder.Entity<Equipo>(entity =>
            {
                entity.Property(e => e.Celular).IsRequired();

                entity.Property(e => e.CelularNormalizado)
                    .IsRequired()
                    .HasColumnName("Celular_Normalizado");

                entity.Property(e => e.Correo).IsRequired();

                entity.Property(e => e.CorreoNormalizado)
                    .IsRequired()
                    .HasColumnName("Correo_Normalizado");

                entity.Property(e => e.FechaCreacion).HasColumnName("Fecha_Creacion");

                entity.Property(e => e.IdCategoria).HasColumnName("Id_Categoria");

                entity.Property(e => e.IdUsuario)
                    .IsRequired()
                    .HasColumnName("Id_Usuario");

                entity.Property(e => e.Nombre).IsRequired();

                entity.Property(e => e.NombreNomalizado)
                    .IsRequired()
                    .HasColumnName("Nombre_Nomalizado");

                entity.Property(e => e.Representante).IsRequired();

                entity.Property(e => e.RepresentanteNormalizado)
                    .IsRequired()
                    .HasColumnName("Representante_Normalizado");

                entity.Property(e => e.RepresentanteOpcional).HasColumnName("Representante_Opcional");

                entity.Property(e => e.RepresentanteOpcionalNormalizado).HasColumnName("Representante_Opcional_Normalizado");

                entity.Property(e => e.TelefonoNormalizado).HasColumnName("Telefono_Normalizado");

                entity.HasOne(d => d.IdCategoriaNavigation)
                    .WithMany(p => p.Equipo)
                    .HasPrincipalKey(p => p.Id)
                    .HasForeignKey(d => d.IdCategoria)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("IdCategoriaEquipo_Categoria");

                entity.HasOne(d => d.IdUsuarioNavigation)
                    .WithMany(p => p.Equipo)
                    .HasForeignKey(d => d.IdUsuario)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("IdUsuarioEquipo_AspNetUsers");
            });

            modelBuilder.Entity<Jugador>(entity =>
            {
                entity.Property(e => e.FechaCreacion).HasColumnName("Fecha_Creacion");

                entity.Property(e => e.IdEquipo).HasColumnName("Id_Equipo");

                entity.Property(e => e.ImagenBase64).HasColumnName("Imagen_Base_64");

                entity.Property(e => e.IsSocio).HasColumnName("Is_Socio");

                entity.Property(e => e.Nombre).IsRequired();

                entity.Property(e => e.NombreNormalizado)
                    .IsRequired()
                    .HasColumnName("Nombre_Normalizado");

                entity.Property(e => e.Serie).HasMaxLength(5);

                entity.HasOne(d => d.IdEquipoNavigation)
                    .WithMany(p => p.Jugador)
                    .HasForeignKey(d => d.IdEquipo)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("IdEquipoJugador_Equipo");
            });

            modelBuilder.Entity<Lugar>(entity =>
            {
                entity.Property(e => e.Nombre).IsRequired();
            });

            modelBuilder.Entity<Partido>(entity =>
            {
                entity.Property(e => e.FechaCreacion).HasColumnName("Fecha_Creacion");

                entity.Property(e => e.FechaPartido).HasColumnName("Fecha_Partido");

                entity.Property(e => e.IdArbitro)
                    .IsRequired()
                    .HasColumnName("Id_Arbitro");

                entity.Property(e => e.IdEquipoLocal).HasColumnName("Id_Equipo_Local");

                entity.Property(e => e.IdEquipoVisitante).HasColumnName("Id_Equipo_Visitante");

                entity.Property(e => e.IdLugar).HasColumnName("Id_Lugar");

                entity.Property(e => e.IdUsuarioCreador)
                    .IsRequired()
                    .HasColumnName("Id_Usuario_Creador");

                entity.HasOne(d => d.IdArbitroNavigation)
                    .WithMany(p => p.PartidoIdArbitroNavigation)
                    .HasForeignKey(d => d.IdArbitro)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("IdArbitroPartido_AspNetUsers");

                entity.HasOne(d => d.IdEquipoLocalNavigation)
                    .WithMany(p => p.PartidoIdEquipoLocalNavigation)
                    .HasForeignKey(d => d.IdEquipoLocal)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("IdEquipoLocal_Equipo");

                entity.HasOne(d => d.IdEquipoVisitanteNavigation)
                    .WithMany(p => p.PartidoIdEquipoVisitanteNavigation)
                    .HasForeignKey(d => d.IdEquipoVisitante)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("IdEquipoVisitante_Equipo");

                entity.HasOne(d => d.IdLugarNavigation)
                    .WithMany(p => p.Partido)
                    .HasForeignKey(d => d.IdLugar)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("IdLugar_Lugar");

                entity.HasOne(d => d.IdUsuarioCreadorNavigation)
                    .WithMany(p => p.PartidoIdUsuarioCreadorNavigation)
                    .HasForeignKey(d => d.IdUsuarioCreador)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("IdUsuarioCreador_AspNetUsers");
            });

            modelBuilder.Entity<PartidoEvidencias>(entity =>
            {
                entity.ToTable("Partido_Evidencias");

                entity.Property(e => e.FechaCreacion).HasColumnName("Fecha_Creacion");

                entity.Property(e => e.IdPartido).HasColumnName("Id_Partido");

                entity.Property(e => e.IdUsuarioCreador)
                    .IsRequired()
                    .HasColumnName("Id_Usuario_Creador");

                entity.Property(e => e.UrlLocal)
                    .IsRequired()
                    .HasColumnName("Url_Local");

                entity.Property(e => e.UrlVisita)
                    .IsRequired()
                    .HasColumnName("URL_Visita");

                entity.HasOne(d => d.IdPartidoNavigation)
                    .WithMany(p => p.PartidoEvidencias)
                    .HasForeignKey(d => d.IdPartido)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_IdPartido_Partido");

                entity.HasOne(d => d.IdUsuarioCreadorNavigation)
                    .WithMany(p => p.PartidoEvidencias)
                    .HasForeignKey(d => d.IdUsuarioCreador)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_IdUsuarioCreador_AspNetUsers");
            });

            modelBuilder.Entity<PartidoJugadores>(entity =>
            {
                entity.ToTable("Partido_Jugadores");

                entity.Property(e => e.Id).HasDefaultValueSql("nextval('\"Parido_Jugadores_Id_seq\"'::regclass)");

                entity.Property(e => e.FechaCreacion).HasColumnName("Fecha_Creacion");

                entity.Property(e => e.IdJugador).HasColumnName("Id_Jugador");

                entity.Property(e => e.IdPartido).HasColumnName("Id_Partido");

                entity.HasOne(d => d.IdJugadorNavigation)
                    .WithMany(p => p.PartidoJugadores)
                    .HasForeignKey(d => d.IdJugador)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("IdJugadorPartido_Jugador");

                entity.HasOne(d => d.IdPartidoNavigation)
                    .WithMany(p => p.PartidoJugadores)
                    .HasForeignKey(d => d.IdPartido)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("IdPartidoPartido_Jugador");
            });

            modelBuilder.Entity<PartidoResultados>(entity =>
            {
                entity.ToTable("Partido_Resultados");

                entity.Property(e => e.FechaCreacion).HasColumnName("Fecha_Creacion");

                entity.Property(e => e.IdPartido).HasColumnName("Id_Partido");

                entity.Property(e => e.PuntosLocal).HasColumnName("Puntos_Local");

                entity.Property(e => e.PuntosVisitante).HasColumnName("Puntos_Visitante");

                entity.Property(e => e.Set).IsRequired();

                entity.HasOne(d => d.IdPartidoNavigation)
                    .WithMany(p => p.PartidoResultados)
                    .HasForeignKey(d => d.IdPartido)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("Id_Partido_EquipoResultados_Partido");
            });

            modelBuilder.Entity<Seccion>(entity =>
            {
                entity.Property(e => e.Nombre).IsRequired();
            });

            modelBuilder.HasSequence<int>("Parido_Jugadores_Id_seq");
        }
    }
}
