using System;
using System.Collections.Generic;
using System.Linq;
using SAN_ISIDRO.DataAccessPSQL;
using SAN_ISIDRO.Models;
using SAN_ISIDRO.Helpers;
using System.Text.RegularExpressions;
using Microsoft.EntityFrameworkCore;
using CloudinaryDotNet;
using CloudinaryDotNet.Actions;

namespace SAN_ISIDRO.Brigde
{
    public class TorneoBridge
    {
        AuthBridge auth;
        public TorneoBridge()
        {
            auth = new AuthBridge();
        }
        public string RegistrarEquipo(string UserName, Models.Equipo equipo, List<Models.Jugador> jugadores)
        {
            using (var db = new PTK_SAN_ISIDROContext())
            {
                using (var transaccion = db.Database.BeginTransaction())
                {
                    try
                    {
                        var onlynumber = new Regex(@"[^\d]");

                        var EquipoDB = new DataAccessPSQL.Equipo();
                        EquipoDB.IdUsuario = auth.IdUsuario(UserName);
                        EquipoDB.Nombre = equipo.NombreEquipo;
                        EquipoDB.NombreNomalizado = StringHelpers.Normalized(equipo.NombreEquipo);
                        EquipoDB.Representante = equipo.Representante;
                        EquipoDB.RepresentanteNormalizado = StringHelpers.Normalized(equipo.Representante);
                        EquipoDB.RepresentanteOpcional = !string.IsNullOrEmpty(equipo.RepresentanteAlterno) ? equipo.RepresentanteAlterno : null;
                        EquipoDB.RepresentanteOpcionalNormalizado = !string.IsNullOrEmpty(equipo.RepresentanteAlterno) ? StringHelpers.Normalized(equipo.RepresentanteAlterno) : null;

                        var celularFormat = equipo.Celular;
                        var normalizedCelular = onlynumber.Replace(equipo.Celular, "");
                        var numberCelular = Convert.ToDouble(normalizedCelular);
                        if (celularFormat.Length <= 10)
                        {
                            celularFormat = String.Format("{0:(###) ###-####}", numberCelular);
                        }
                        else if (celularFormat.Length > 10)
                        {
                            celularFormat = String.Format("{0:## (###) ###-####}", numberCelular);
                        }

                        EquipoDB.Celular = celularFormat;
                        EquipoDB.CelularNormalizado = normalizedCelular;

                        if (!string.IsNullOrEmpty(equipo.Telefono))
                        {
                            var telefonoFormat = equipo.Telefono;
                            var normalizedTelefono = onlynumber.Replace(equipo.Telefono, "");
                            var numberTelefono = Convert.ToDouble(normalizedTelefono);
                            if (telefonoFormat.Length <= 10)
                            {
                                telefonoFormat = String.Format("{0:(###) ###-####}", numberTelefono);
                            }
                            else if (telefonoFormat.Length > 10)
                            {
                                telefonoFormat = String.Format("{0:## (###) ###-####}", numberTelefono);
                            }

                            EquipoDB.Telefono = telefonoFormat;
                            EquipoDB.TelefonoNormalizado = normalizedTelefono;
                        }

                        EquipoDB.Correo = equipo.Email.Trim().ToLower();
                        EquipoDB.CorreoNormalizado = equipo.Email.Trim().ToUpper();

                        EquipoDB.FechaCreacion = DateTime.Now;

                        EquipoDB.IdCategoria = equipo.IdCategoria;

                        db.Equipo.Add(EquipoDB);
                        db.SaveChanges();

                        var idEquipo = EquipoDB.Id;

                        var instanseCloudBinary = GetInstanceCloudBinary();

                        foreach (var jug in jugadores)
                        {
                            var uploadParams = new ImageUploadParams()
                            {
                                File = new FileDescription(jug.img),
                                Transformation = new Transformation().Quality("auto"),
                                Folder = "SAN_ISIDRO"
                            };
                            var uploadResult = instanseCloudBinary.Upload(uploadParams);

                            string url = uploadResult.SecureUri.AbsoluteUri;

                            db.Jugador.Add(new DataAccessPSQL.Jugador
                            {
                                Nombre = jug.nombre,
                                NombreNormalizado = StringHelpers.Normalized(jug.nombre),
                                IsSocio = jug.isSocio,
                                Serie = !string.IsNullOrEmpty(jug.serie) ? jug.serie : null,
                                //ImagenBase64 = jug.img,
                                Url = url,
                                FechaCreacion = DateTime.Now,
                                IdEquipo = idEquipo
                            });
                        }

                        db.SaveChanges();

                        transaccion.Commit();

                        return null;
                    }
                    catch (Exception e)
                    {
                        transaccion.Rollback();
                        return e.ToString();
                    }
                }
            }
        }

        public string Actualizar(string UserName, int IdEquipo, Models.Equipo equipo, List<Models.Jugador> jugadores)
        {
            using (var db = new PTK_SAN_ISIDROContext())
            {
                using (var transaccion = db.Database.BeginTransaction())
                {
                    try
                    {
                        var onlynumber = new Regex(@"[^\d]");

                        var EquipoDB = db.Equipo.First(x => x.Id == IdEquipo);
                        EquipoDB.Nombre = equipo.NombreEquipo;
                        EquipoDB.NombreNomalizado = StringHelpers.Normalized(equipo.NombreEquipo);
                        EquipoDB.Representante = equipo.Representante;
                        EquipoDB.RepresentanteNormalizado = StringHelpers.Normalized(equipo.Representante);
                        EquipoDB.RepresentanteOpcional = !string.IsNullOrEmpty(equipo.RepresentanteAlterno) ? equipo.RepresentanteAlterno : null;
                        EquipoDB.RepresentanteOpcionalNormalizado = !string.IsNullOrEmpty(equipo.RepresentanteAlterno) ? StringHelpers.Normalized(equipo.RepresentanteAlterno) : null;

                        var celularFormat = equipo.Celular;
                        var normalizedCelular = onlynumber.Replace(equipo.Celular, "");
                        var numberCelular = Convert.ToDouble(normalizedCelular);
                        if (celularFormat.Length <= 10)
                        {
                            celularFormat = String.Format("{0:(###) ###-####}", numberCelular);
                        }
                        else if (celularFormat.Length > 10)
                        {
                            celularFormat = String.Format("{0:## (###) ###-####}", numberCelular);
                        }

                        EquipoDB.Celular = celularFormat;
                        EquipoDB.CelularNormalizado = normalizedCelular;

                        if (!string.IsNullOrEmpty(equipo.Telefono))
                        {
                            var telefonoFormat = equipo.Telefono;
                            var normalizedTelefono = onlynumber.Replace(equipo.Telefono, "");
                            var numberTelefono = Convert.ToDouble(normalizedTelefono);
                            if (telefonoFormat.Length <= 10)
                            {
                                telefonoFormat = String.Format("{0:(###) ###-####}", numberTelefono);
                            }
                            else if (telefonoFormat.Length > 10)
                            {
                                telefonoFormat = String.Format("{0:## (###) ###-####}", numberTelefono);
                            }

                            EquipoDB.Telefono = telefonoFormat;
                            EquipoDB.TelefonoNormalizado = normalizedTelefono;
                        }

                        EquipoDB.Correo = equipo.Email.Trim().ToLower();
                        EquipoDB.CorreoNormalizado = equipo.Email.Trim().ToUpper();

                        EquipoDB.FechaCreacion = DateTime.Now;

                        EquipoDB.IdCategoria = equipo.IdCategoria;

                        db.SaveChanges();

                        foreach (var jug in db.Jugador.Where(x => x.IdEquipo == IdEquipo))
                        {
                            db.Jugador.Remove(jug);
                        }
                        db.SaveChanges();

                        var instanseCloudBinary = GetInstanceCloudBinary();

                        foreach (var jug in jugadores)
                        {
                            var uploadParams = new ImageUploadParams()
                            {
                                File = new FileDescription(jug.img),
                                Transformation = new Transformation().Quality("auto"),
                                Folder = "SAN_ISIDRO"
                            };
                            var uploadResult = instanseCloudBinary.Upload(uploadParams);

                            string url = uploadResult.SecureUri.AbsoluteUri;

                            db.Jugador.Add(new DataAccessPSQL.Jugador
                            {
                                Nombre = jug.nombre,
                                NombreNormalizado = StringHelpers.Normalized(jug.nombre),
                                IsSocio = jug.isSocio,
                                Serie = !string.IsNullOrEmpty(jug.serie) ? jug.serie : null,
                                //ImagenBase64 = jug.img,
                                Url = url,
                                FechaCreacion = DateTime.Now,
                                IdEquipo = IdEquipo
                            });
                            db.SaveChanges();
                        }


                        transaccion.Commit();

                        return null;
                    }
                    catch (Exception e)
                    {
                        transaccion.Rollback();
                        return e.ToString();
                    }
                }
            }
        }

        public Models.Equipo GetEquipo(string UserName)
        {
            using (var db = new PTK_SAN_ISIDROContext())
            {
                var equipoDB = db.Equipo
                            .Include(x => x.Jugador)
                            .Include(x => x.IdCategoriaNavigation)
                            .Include(x => x.IdUsuarioNavigation)
                            .First(x => x.IdUsuarioNavigation.UserName == UserName);

                var equipo = new Models.Equipo()
                {
                    Id = equipoDB.Id,
                    NombreEquipo = equipoDB.Nombre,
                    Categoria = equipoDB.IdCategoriaNavigation.Nombre,
                    IdCategoria = equipoDB.IdCategoria,
                    Representante = equipoDB.Representante,
                    RepresentanteAlterno = equipoDB.RepresentanteOpcional ?? null,
                    Celular = equipoDB.Celular,
                    Email = equipoDB.Correo,
                    Telefono = equipoDB.Telefono ?? null,
                    Jugadores = equipoDB.Jugador.Select(x => new Models.Jugador
                    {
                        nombre = x.Nombre,
                        isSocio = x.IsSocio,
                        serie = x.Serie ?? null,
                        img = x.Url
                    }).ToList()
                };

                return equipo;
            }
        }

        public Models.Equipo GetEquipo(string UserName, int IdEquipo)
        {
            using (var db = new PTK_SAN_ISIDROContext())
            {
                var equipoDB = db.Equipo
                            .Include(x => x.Jugador)
                            .Include(x => x.IdCategoriaNavigation)
                            .Include(x => x.IdUsuarioNavigation)
                            .First(x => x.Id == IdEquipo);

                var equipo = new Models.Equipo()
                {
                    Id = equipoDB.Id,
                    NombreEquipo = equipoDB.Nombre,
                    Categoria = equipoDB.IdCategoriaNavigation.Nombre,
                    IdCategoria = equipoDB.IdCategoria,
                    Representante = equipoDB.Representante,
                    RepresentanteAlterno = equipoDB.RepresentanteOpcional ?? null,
                    Celular = equipoDB.Celular,
                    Email = equipoDB.Correo,
                    Telefono = equipoDB.Telefono ?? null,
                    Jugadores = equipoDB.Jugador.Select(x => new Models.Jugador
                    {
                        nombre = x.Nombre,
                        isSocio = x.IsSocio,
                        serie = x.Serie ?? null,
                        img = x.Url
                    }).ToList()
                };

                return equipo;
            }
        }

        public Cloudinary GetInstanceCloudBinary()
        {
            using (var db = new PTK_SAN_ISIDROContext())
            {
                Account account = new Account(
                    "dqdah2u0l",
                    "982673399137531",
                    "oJZ6DZbz5zfWdiBUUue9El283tc");

                Cloudinary cloudinary = new Cloudinary(account);

                return cloudinary;

                // foreach (var jug in db.Jugador)
                // {
                //     var uploadParams = new ImageUploadParams()
                //     {
                //         File = new FileDescription(jug.ImagenBase64),
                //         Transformation = new Transformation().Quality("auto"),
                //         Folder = "SAN_ISIDRO"
                //     };
                //     var uploadResult = cloudinary.Upload(uploadParams);

                //     string url = uploadResult.SecureUri.AbsoluteUri;

                //     jug.Url = url;     
                // }
                // db.SaveChanges();
            }
        }
    }
}