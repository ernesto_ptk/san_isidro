using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using CloudinaryDotNet;
using CloudinaryDotNet.Actions;
using Microsoft.EntityFrameworkCore;
using SAN_ISIDRO.DataAccessPSQL;
using SAN_ISIDRO.Models;

namespace SAN_ISIDRO.Brigde
{
    public class PartidoBridge
    {
        AuthBridge auth;
        public PartidoBridge()
        {
            auth = new AuthBridge();
        }

        public List<Models.Partido> GetPartidos()
        {
            using (var db = new PTK_SAN_ISIDROContext())
            {

                try
                {
                    var partidos = db.Partido.Select(x => new Models.Partido
                    {
                        Local = new Models.Equipo
                        {
                            NombreEquipo = x.IdEquipoLocalNavigation.Nombre
                        },
                        Visitante = new Models.Equipo
                        {
                            NombreEquipo = x.IdEquipoVisitanteNavigation.Nombre
                        },
                        Fecha = x.FechaPartido.ToString("ddd dd MMM yyyy"),
                        Horario = x.FechaPartido.ToString("hh:mm ttt"),
                        Categoria = new Models.Categoria
                        {
                            Nombre = x.IdEquipoLocalNavigation.IdCategoriaNavigation.Nombre
                        },
                        Lugar = new Models.Lugar
                        {
                            Nombre = x.IdLugarNavigation.Nombre
                        },
                        Arbitro = new Models.Usuario
                        {
                            NombreUsuario = x.IdArbitroNavigation.UserName
                        }
                    }).ToList();

                    return partidos;
                }
                catch (Exception)
                {
                    return null;
                }
            }
        }

        public List<Models.Partido> GetPartidosForArbitro(string UserName)
        {
            using (var db = new PTK_SAN_ISIDROContext())
            {
                try
                {
                    var partidosDb = db.Partido
                        .Include(x => x.IdArbitroNavigation)
                        .Where(x => x.IdArbitroNavigation.UserName == UserName);

                    if (partidosDb.Count() > 0)
                    {
                        return partidosDb.Select(x => new Models.Partido
                        {
                            Id = x.Id,
                            Local = new Models.Equipo
                            {
                                NombreEquipo = x.IdEquipoLocalNavigation.Nombre
                            },
                            Visitante = new Models.Equipo
                            {
                                NombreEquipo = x.IdEquipoVisitanteNavigation.Nombre
                            },
                            Fecha = x.FechaPartido.ToString("ddd dd MMM yyyy"),
                            Horario = x.FechaPartido.ToString("hh:mm ttt"),
                            Categoria = new Models.Categoria
                            {
                                Nombre = x.IdEquipoLocalNavigation.IdCategoriaNavigation.Nombre
                            },
                            Lugar = new Models.Lugar
                            {
                                Nombre = x.IdLugarNavigation.Nombre
                            },
                            Arbitro = new Models.Usuario
                            {
                                NombreUsuario = x.IdArbitroNavigation.UserName
                            }
                        }).ToList();
                    }
                    else
                    {
                        return new List<Models.Partido>();
                    }
                }
                catch (Exception)
                {
                    return new List<Models.Partido>();
                }
            }
        }

        public string RegistrarPartido(string UserName, Models.Partido partido)
        {
            using (var db = new PTK_SAN_ISIDROContext())
            {
                using (var transaccion = db.Database.BeginTransaction())
                {
                    try
                    {
                        var fecha = DateTime.ParseExact(partido.Fecha, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                        var hora = DateTime.ParseExact(partido.Horario, "hh:mm tt", CultureInfo.InvariantCulture);

                        var fechaPartido = new DateTime(fecha.Year, fecha.Month, fecha.Day, hora.Hour, hora.Minute, 0);

                        if (db.Partido.FirstOrDefault(x => x.FechaPartido == fechaPartido && x.IdLugar == 1) == null)
                        {
                            var partidoDb = new DataAccessPSQL.Partido();
                            partidoDb.IdEquipoLocal = partido.IdLocal;
                            partidoDb.IdEquipoVisitante = partido.IdVisitante;
                            partidoDb.FechaCreacion = DateTime.Now;
                            partidoDb.FechaPartido = fechaPartido;
                            partidoDb.IdLugar = 1;
                            partidoDb.IdArbitro = auth.IdUsuario(UserName);
                            partidoDb.IdUsuarioCreador = auth.IdUsuario(UserName);

                            db.Partido.Add(partidoDb);
                            db.SaveChanges();
                            transaccion.Commit();

                            return null;
                        }
                        else
                        {
                            transaccion.Rollback();
                            return $"No es posible definir un partido en esta Fecha {fechaPartido.ToString("dd/MM/yyyy")}, a la hora de {fechaPartido.ToString("hh:mm tt")} y en el lugar definido porque ya existe uno previamente.";
                        }
                    }
                    catch (Exception e)
                    {
                        transaccion.Rollback();
                        return e.ToString();
                    }
                }
            }
        }

        public async Task<string> RegistrarEvidenciaPartido(string UserName, int IdPartido, string ImgLocal, string ImgVisita)
        {
            using (var db = new PTK_SAN_ISIDROContext())
            {
                using (var transaccion = db.Database.BeginTransaction())
                {
                    try
                    {
                        if (db.Partido.FirstOrDefault(x => x.Id == IdPartido) == null)
                        {
                            var instanseCloudBinary = GetInstanceCloudBinary();

                            var uploadParamsLocal = new ImageUploadParams()
                            {
                                File = new FileDescription(ImgLocal),
                                Transformation = new Transformation().Quality("auto"),
                                Folder = "SAN_ISIDRO_EVIDENCIA"
                            };
                            var uploadResultLocal = instanseCloudBinary.Upload(uploadParamsLocal);

                            string url_local = uploadResultLocal.SecureUri.AbsoluteUri;

                            var uploadParamsVisita = new ImageUploadParams()
                            {
                                File = new FileDescription(ImgVisita),
                                Transformation = new Transformation().Quality("auto"),
                                Folder = "SAN_ISIDRO_EVIDENCIA"
                            };
                            var uploadResultVisita = instanseCloudBinary.Upload(uploadParamsVisita);

                            string url_visita = uploadResultVisita.SecureUri.AbsoluteUri;

                            var partidoEvidenciaDb = new DataAccessPSQL.PartidoEvidencias();
                            partidoEvidenciaDb.IdPartido = IdPartido;
                            partidoEvidenciaDb.UrlLocal = url_local;
                            partidoEvidenciaDb.UrlVisita = url_visita;
                            partidoEvidenciaDb.IdUsuarioCreador = auth.IdUsuario(UserName);
                            partidoEvidenciaDb.FechaCreacion = DateTime.Now;

                            db.PartidoEvidencias.Add(partidoEvidenciaDb);
                            await db.SaveChangesAsync();
                            transaccion.Commit();

                            return null;
                        }
                        else
                        {
                            transaccion.Rollback();
                            return $"No es posible definir un partido que no existe en el sistema, verificar con el Administrador.";
                        }
                    }
                    catch (Exception e)
                    {
                        transaccion.Rollback();
                        return e.ToString();
                    }
                }
            }
        }

        public async Task<string> RegistrarEvidenciaPartido(string UserName, int IdPartido, List<int> JugadoresLocal, List<int> JugadoresVisita)
        {
            using (var db = new PTK_SAN_ISIDROContext())
            {
                using (var transaccion = db.Database.BeginTransaction())
                {
                    try
                    {
                        if (db.Partido.FirstOrDefault(x => x.Id == IdPartido) == null)
                        {
                            var partidoJugadoresDb = new DataAccessPSQL.PartidoJugadores();
                            foreach (var jugadorLcl in JugadoresLocal)
                            {
                                db.PartidoJugadores.Add(new PartidoJugadores
                                {
                                    IdPartido = IdPartido,
                                    IdJugador = jugadorLcl,
                                    FechaCreacion = DateTime.Now
                                });
                            }
                            db.SaveChanges();

                            foreach (var jugadorVis in JugadoresVisita)
                            {
                                db.PartidoJugadores.Add(new PartidoJugadores
                                {
                                    IdPartido = IdPartido,
                                    IdJugador = jugadorVis,
                                    FechaCreacion = DateTime.Now
                                });
                            }

                            await db.SaveChangesAsync();
                            transaccion.Commit();

                            return null;
                        }
                        else
                        {
                            transaccion.Rollback();
                            return $"No es posible definir un partido que no existe en el sistema, verificar con el Administrador.";
                        }
                    }
                    catch (Exception e)
                    {
                        transaccion.Rollback();
                        return e.ToString();
                    }
                }
            }
        }

        public CedulaPartido GetCedulaPartido(int IdPartido)
        {
            using (var db = new PTK_SAN_ISIDROContext())
            {
                var partido = db.Partido
                            .Include(x => x.IdArbitroNavigation)
                            .Include(x => x.IdLugarNavigation)
                            .Include(x => x.IdEquipoLocalNavigation)
                                .ThenInclude(x => x.Jugador)
                            .Include(x => x.IdEquipoVisitanteNavigation)
                                .ThenInclude(x => x.Jugador)
                            .First(x => x.Id == IdPartido);

                var cedulaPartido = new CedulaPartido();
                cedulaPartido.Id = partido.Id;
                cedulaPartido.Local = partido.IdEquipoLocalNavigation.Nombre;
                cedulaPartido.RepresentanteLocal = partido.IdEquipoLocalNavigation.Representante;

                cedulaPartido.Visitante = partido.IdEquipoVisitanteNavigation.Nombre;
                cedulaPartido.RepresentanteVisitante = partido.IdEquipoVisitanteNavigation.Representante;

                cedulaPartido.Fecha = partido.FechaPartido.ToString("dd/MM/yyyy");
                cedulaPartido.Hora = partido.FechaPartido.ToString("hh:mm tt");

                cedulaPartido.Lugar = partido.IdLugarNavigation.Nombre;
                cedulaPartido.Arbitro = partido.IdArbitroNavigation.UserName;

                cedulaPartido.EquipoLocal = partido.IdEquipoLocalNavigation.Jugador.Select(x => new Models.Jugador
                {
                    id = x.Id,
                    nombre = x.Nombre,
                    isSocio = x.IsSocio,
                    serie = x.Serie,
                    img = x.Url
                }).ToList();

                cedulaPartido.EquipoVisitante = partido.IdEquipoVisitanteNavigation.Jugador.Select(x => new Models.Jugador
                {
                    id = x.Id,
                    nombre = x.Nombre,
                    isSocio = x.IsSocio,
                    serie = x.Serie,
                    img = x.Url
                }).ToList();

                return cedulaPartido;

            }
        }

        public Cloudinary GetInstanceCloudBinary()
        {
            using (var db = new PTK_SAN_ISIDROContext())
            {
                Account account = new Account(
                    "dqdah2u0l",
                    "982673399137531",
                    "oJZ6DZbz5zfWdiBUUue9El283tc");

                Cloudinary cloudinary = new Cloudinary(account);

                return cloudinary;

                // foreach (var jug in db.Jugador)
                // {
                //     var uploadParams = new ImageUploadParams()
                //     {
                //         File = new FileDescription(jug.ImagenBase64),
                //         Transformation = new Transformation().Quality("auto"),
                //         Folder = "SAN_ISIDRO"
                //     };
                //     var uploadResult = cloudinary.Upload(uploadParams);

                //     string url = uploadResult.SecureUri.AbsoluteUri;

                //     jug.Url = url;     
                // }
                // db.SaveChanges();
            }
        }
    }
}