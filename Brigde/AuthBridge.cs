using System;
using System.Linq;

namespace SAN_ISIDRO.Brigde
{
    using SAN_ISIDRO.DataAccessPSQL;
    using SAN_ISIDRO;
    using System.Collections.Generic;
    using SAN_ISIDRO.Models;
    using Microsoft.EntityFrameworkCore;

    public class AuthBridge
    {
        public string IdUsuario(string UserName)
        {
            using (var db = new PTK_SAN_ISIDROContext())
            {
                return db.AspNetUsers.First(x => x.UserName == UserName).Id;
            }
        }

        public List<Usuario> GetUsers()
        {
            using (var db = new PTK_SAN_ISIDROContext())
            {
                try
                {
                    var UsuarioDb = db.AspNetUsers
                    .Include(x => x.Equipo)
                    .ThenInclude(x => x.IdCategoriaNavigation)
                    .Where(x => !x.AspNetUserRoles.Any(y => y.RoleId == "77956839-753a-4e47-8e11-ff7a9a64e010"))
                    .Select(x =>
                    new Usuario
                    {
                        NombreUsuario = x.UserName,
                        Equipo = x.Equipo.FirstOrDefault() != null ? new Models.Equipo
                        {
                            Id = x.Equipo.First().Id,
                            NombreEquipo = x.Equipo.First().Nombre,
                            Categoria = x.Equipo.First().IdCategoriaNavigation.Nombre,
                            FechaCreacion = x.Equipo.First().FechaCreacion
                        } : null
                    });
                    return UsuarioDb.ToList();
                }
                catch (Exception)
                {
                    return null;
                }
            }
        }

        public List<Usuario> GetArbitros()
        {
            using (var db = new PTK_SAN_ISIDROContext())
            {
                try
                {
                    // var i = 1;
                    var UsuarioDb = db.AspNetUsers
                    .Include(x => x.AspNetUserRoles)
                    .Where(x => x.AspNetUserRoles.Any(y => y.RoleId == "77956839-753a-4e47-8e11-ff7a9a64e010"))
                    .Select(x =>
                    new Usuario
                    {
                        NombreUsuario = x.UserName,
                    });
                    return UsuarioDb.ToList();
                }
                catch (Exception)
                {
                    return null;
                }
            }
        }
    }
}