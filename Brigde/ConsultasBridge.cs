using System;
using System.Collections.Generic;
using System.Linq;
using CloudinaryDotNet;
using CloudinaryDotNet.Actions;
using SAN_ISIDRO.DataAccessPSQL;
using SAN_ISIDRO.Models;

namespace SAN_ISIDRO.Brigde
{
    public class ConsultasBridge
    {
        public List<Models.Categoria> GetCategorias()
        {
            using (var db = new PTK_SAN_ISIDROContext())
            {
                return db.Categoria.Select(x => new Models.Categoria
                {
                    Id = x.Id,
                    Nombre = x.Nombre
                }).ToList();
            }
        }

        public List<Models.Lugar> GetLugares()
        {
            using (var db = new PTK_SAN_ISIDROContext())
            {
                return db.Lugar.Select(x => new Models.Lugar
                {
                    Id = x.Id,
                    Nombre = x.Nombre
                }).ToList();
            }
        }

        public bool IsRegisterEquipo(string UserName)
        {
            using (var db = new PTK_SAN_ISIDROContext())
            {
                return db.Equipo.FirstOrDefault(x => x.IdUsuarioNavigation.UserName == UserName) != null;
            }
        }

        public List<Models.Equipo> GetEquipoWithOut(int IdCategoria, int IdEquipo)
        {
            using (var db = new PTK_SAN_ISIDROContext())
            {
                return db.Equipo.Where(x => x.IdCategoria == IdCategoria && (IdEquipo == 0 ? true : x.Id != IdEquipo))
                .Select(x => new Models.Equipo
                {
                    Id = x.Id,
                    NombreEquipo = x.Nombre
                }).ToList();
            }
        }
    }
}