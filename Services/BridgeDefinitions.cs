using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using SAN_ISIDRO.Brigde;

namespace SAN_ISIDRO.Services
{
    
    public interface IBridgeSingleton<T>
    {
        T Bridge { get; set; }
    }

    public class Singleton<T> where T : new()
    {
        T instnce;

        public static Singleton<T> GetInstance()
        {

            return new Singleton<T>();
        }

        public T Instance
        {
            get
            {
                if (instnce == null)
                {
                    instnce = new T();
                }
                return instnce;
            }
            set
            {
                instnce = value;
            }
        }
    }

    public interface IBridgeConsultas : IBridgeSingleton<ConsultasBridge>
    {
    }
    public class SingletonConsultasBridge : IBridgeConsultas
    {
        ConsultasBridge bridge;
        public ConsultasBridge Bridge
        {
            get
            {
                if (bridge == null)
                {
                    bridge = new ConsultasBridge();
                }
                return bridge;
            }
            set { bridge = value; }
        }
    }

    public interface IBridgeTorneo : IBridgeSingleton<TorneoBridge>
    {
    }
    public class SingletonTorneoBridge : IBridgeTorneo
    {
        TorneoBridge bridge;
        public TorneoBridge Bridge
        {
            get
            {
                if (bridge == null)
                {
                    bridge = new TorneoBridge();
                }
                return bridge;
            }
            set { bridge = value; }
        }
    }

    public interface IBridgePartido : IBridgeSingleton<PartidoBridge>
    {
    }
    public class SingletonPartidoBridge : IBridgePartido
    {
        PartidoBridge bridge;
        public PartidoBridge Bridge
        {
            get
            {
                if (bridge == null)
                {
                    bridge = new PartidoBridge();
                }
                return bridge;
            }
            set { bridge = value; }
        }
    }
}