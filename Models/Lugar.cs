namespace SAN_ISIDRO.Models
{
    public class Lugar
    {
        public int Id { get; set; }
        public string Nombre { get; set; }
    }
}