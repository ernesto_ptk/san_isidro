using System;
using System.Collections.Generic;

namespace SAN_ISIDRO.Models
{
    public class Query
    {
        public int page { get; set; }
        public int take { get; set; } = 15;
        public int orderBy { get; set; } = 0;
    }
}