using System.Collections.Generic;
namespace SAN_ISIDRO.Models
{
    public class CedulaPartido
    {
        public CedulaPartido(){
            EquipoLocal = new List<Jugador>();
            EquipoVisitante = new List<Jugador>();
        }

        public int Id { get; set; }
        public string Local { get; set; }
        public string RepresentanteLocal { get; set; }
        public string Visitante { get; set; }
        public string RepresentanteVisitante { get; set; }
        public string Fecha { get; set; }
        public string Hora { get; set; }
        public string Lugar { get; set; }
        public string Arbitro { get; set; }

        public List<Jugador> EquipoLocal { get; set; }
        public List<Jugador> EquipoVisitante { get; set; }
    }
}