using System.Collections.Generic;

namespace SAN_ISIDRO.Models.ViewModels
{
    public class AdministracionViewModel
    {
        public AdministracionViewModel(){
            Usuarios = new List<Usuario>();
            Arbitros = new List<Usuario>();
        }
        public List<Usuario> Usuarios { get; set; }
        public List<Usuario> Arbitros { get; set; }

    }
}