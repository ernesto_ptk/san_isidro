using System.Collections.Generic;

namespace SAN_ISIDRO.Models.ViewModels
{
    public class RegistroViewModel
    {
        public RegistroViewModel(){
            Categorias = new List<Categoria>();
        }
        public int IdEquipo { get; set; }
        public List<Categoria> Categorias { get; set; }
        public bool IsCedulaDefined { get; set; }
    }
}