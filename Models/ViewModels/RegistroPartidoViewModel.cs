using System.Collections.Generic;

namespace SAN_ISIDRO.Models.ViewModels
{
    public class RegistroPartidoViewModel
    {
        public List<Categoria> Categorias { get; set; }
        public List<Equipo> EquiposLocal { get; set; }
        public List<Equipo> EquiposVisitante { get; set; }
        public List<Lugar> Lugares { get; set; }
        public List<Usuario> Arbitros { get; set; }
    }
}