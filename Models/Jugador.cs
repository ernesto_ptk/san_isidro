namespace SAN_ISIDRO.Models
{
    public class Jugador
    {
        public int id { get; set; }
        public string nombre { get; set; }
        public bool isSocio { get; set; }
        public string serie { get; set; }
        public string img { get; set; }
    }
}