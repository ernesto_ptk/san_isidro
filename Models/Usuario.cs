using System;

namespace SAN_ISIDRO.Models
{
    public class Usuario
    {
        public string Id { get; set; }
        public string NombreUsuario { get; set; }
        public DateTime FechaCreacion { get; set; }
        public Equipo Equipo { get; set; }
    }
}