namespace SAN_ISIDRO.Models
{
    public class Partido
    {
        public Equipo Local { get; set; }
        public Equipo Visitante { get; set; }
        public int Id { get; set; }
        public int IdLocal { get; set; }
        public int IdVisitante { get; set; }
        public string Fecha { get; set; }
        public string Horario { get; set; }
        public int IdCategoria { get; set; }
        public Categoria Categoria { get; set; }
        public Lugar Lugar { get; set; }
        public Usuario Arbitro { get; set; }
        public bool HasResultados { get; set; }
    }
}