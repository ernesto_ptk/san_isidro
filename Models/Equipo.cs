using System;
using System.Collections.Generic;

namespace SAN_ISIDRO.Models
{
    public class Equipo
    {
        public Equipo()
        {
            Jugadores = new List<Jugador>();
        }
        public int Id { get; set; }
        public string NombreEquipo { get; set; }
        public int IdCategoria { get; set; }
        public string Categoria { get; set; }
        public string Representante { get; set; }
        public string RepresentanteAlterno { get; set; }
        public string Celular { get; set; }
        public string Email { get; set; }
        public string Telefono { get; set; }
        public DateTime FechaCreacion { get; set; }

        public List<Jugador> Jugadores { get; set; }
        
        public int CountJugadores { get; set; }
        public int CountSocios { get; set; }
        public int CountNoSocios { get; set; }
    }
}