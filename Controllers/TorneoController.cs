using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using SAN_ISIDRO.Brigde;
using SAN_ISIDRO.Models.ViewModels;
using SAN_ISIDRO.Services;
using Newtonsoft.Json;

namespace SAN_ISIDRO.Controllers
{
    using System.Collections.Generic;
    using SAN_ISIDRO.Models;

    [Authorize]

    public class TorneoController : Controller
    {
        Singleton<ConsultasBridge> SingletonConsultas;
        Singleton<TorneoBridge> SingletonTorneo;
        public TorneoController()
        {
            SingletonConsultas = Singleton<ConsultasBridge>.GetInstance();
            SingletonTorneo = Singleton<TorneoBridge>.GetInstance();
        }

        [HttpGet]
        public IActionResult Index()
        {
            var vm = new RegistroViewModel();
            vm.IsCedulaDefined = SingletonConsultas.Instance.IsRegisterEquipo(User.Identity.Name);
            vm.Categorias = SingletonConsultas.Instance.GetCategorias();
            ViewData["Title"] = "Cedula de Inscripción";
            return View(vm);
        }

        // [HttpGet]
        // public IActionResult TransformImg()
        // {
        //     SingletonConsultas.Instance.GetUrlImg();
        //     return View();
        // }

        [HttpGet]
        public IActionResult VerEquipo(int IdEquipo)
        {
            var vm = new RegistroViewModel();
            vm.IdEquipo = IdEquipo;
            vm.Categorias = SingletonConsultas.Instance.GetCategorias();
            ViewData["Title"] = "Detalle del Equipo";
            return View(vm);
        }

        [HttpPost]
        public string Registrar([FromBody]dynamic Request)
        {
            var equipo = (Equipo)Request.Equipo.ToObject<Equipo>();
            var jugadores = (List<Jugador>)Request.Jugadores.ToObject<List<Jugador>>();

            var result = SingletonTorneo.Instance.RegistrarEquipo(User.Identity.Name, equipo, jugadores);

            return JsonConvert.SerializeObject(new { result = result == null ? true : false, message = result });
        }

        [HttpPost]
        public string Actualizar([FromBody]dynamic Request)
        {
            var IdEquipo = (int)Request.Id;
            var equipo = (Equipo)Request.Equipo.ToObject<Equipo>();
            var jugadores = (List<Jugador>)Request.Jugadores.ToObject<List<Jugador>>();

            var result = SingletonTorneo.Instance.Actualizar(User.Identity.Name, IdEquipo, equipo, jugadores);

            return JsonConvert.SerializeObject(new { result = result == null ? true : false, message = result });
        }

        [HttpGet]
        public string ObtenerEquipo()
        {
            var result = SingletonTorneo.Instance.GetEquipo(User.Identity.Name);

            return JsonConvert.SerializeObject(new { result });
        }

        [HttpGet]
        public string ObtenerEquipoId(int IdEquipo)
        {
            var result = SingletonTorneo.Instance.GetEquipo(User.Identity.Name, IdEquipo);

            return JsonConvert.SerializeObject(new { result });
        }
    }
}