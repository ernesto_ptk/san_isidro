using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using SAN_ISIDRO.Brigde;
using SAN_ISIDRO.Models.ViewModels;
using SAN_ISIDRO.Services;
using Newtonsoft.Json;
using System.Linq;

namespace SAN_ISIDRO.Controllers
{
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Identity;
    using Microsoft.Extensions.Logging;
    using SAN_ISIDRO.Models;

    [Authorize]
    public class PartidosController : Controller
    {
        private readonly IViewRenderService _viewRenderService;
        Singleton<ConsultasBridge> SingletonConsultas;
        Singleton<AuthBridge> SingletonAuth;
        Singleton<PartidoBridge> SingletonPartido;

        public PartidosController(IViewRenderService viewRenderService)
        {
            _viewRenderService = viewRenderService;
            SingletonConsultas = Singleton<ConsultasBridge>.GetInstance();
            SingletonPartido = Singleton<PartidoBridge>.GetInstance();
            SingletonAuth = Singleton<AuthBridge>.GetInstance();
        }

        [HttpGet]
        public IActionResult Index()
        {
            ViewData["Title"] = "Partidos";
            return View();
        }

        [HttpGet]
        public IActionResult CedulaPartido()
        {
            ViewData["Title"] = "Cedula de Partido";
            return View();
        }

        [HttpGet]
        public async Task<string> GetModalRegistrar()
        {
            var vm = new RegistroPartidoViewModel();
            vm.Categorias = SingletonConsultas.Instance.GetCategorias();
            vm.Lugares = SingletonConsultas.Instance.GetLugares();
            vm.Arbitros = SingletonAuth.Instance.GetArbitros();
            var html = await _viewRenderService.RenderToStringAsync2("", "~/Views/Partidos/AddPartido.cshtml", vm);

            return JsonConvert.SerializeObject(new { html });
        }

        [HttpPost]
        public string GetEquiposByCategoriaAndWithOut([FromBody] dynamic Request)
        {
            var IdCategoria = (int)Request.IdCategoria;
            var IdEquipo = (int)Request.IdEquipoLocal;
            var result = SingletonConsultas.Instance.GetEquipoWithOut(IdCategoria, IdEquipo);
            return JsonConvert.SerializeObject(new { result });
        }

        [HttpPost]
        public string RegistrarEquipo([FromBody] dynamic Request)
        {
            var partido = Request.ToObject<Partido>();
            var result = SingletonPartido.Instance.RegistrarPartido(User.Identity.Name, partido);
            return JsonConvert.SerializeObject(new { result = result == null ? true : false, message = result });
        }

        [HttpPost]
        public async Task<string> GetPartidos([FromBody] dynamic Request)
        {
            var result = SingletonPartido.Instance.GetPartidos();
            var html = await _viewRenderService.RenderToStringAsync2("", "~/Views/Partidos/PartidoMaker.cshtml", result);
            return JsonConvert.SerializeObject(new { html });
        }
    }
}