using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using SAN_ISIDRO.Brigde;
using SAN_ISIDRO.Models.ViewModels;
using SAN_ISIDRO.Services;
using Newtonsoft.Json;
using System.Linq;

namespace SAN_ISIDRO.Controllers
{
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Identity;
    using Microsoft.Extensions.Logging;
    using SAN_ISIDRO.Models;
    [Authorize]
    public class AdministradorController : Controller
    {
        private readonly IViewRenderService _viewRenderService;
        private readonly SignInManager<IdentityUser> _signInManager;
        private readonly UserManager<IdentityUser> _userManager;
        Singleton<AuthBridge> SingletonAuth;

        public AdministradorController(UserManager<IdentityUser> userManager,
            SignInManager<IdentityUser> signInManager, IViewRenderService viewRenderService)
        {
            SingletonAuth = Singleton<AuthBridge>.GetInstance();

            _userManager = userManager;
            _signInManager = signInManager;
            _viewRenderService = viewRenderService;
        }

        [HttpGet]
        public IActionResult Index()
        {
            ViewData["Title"] = "Registrar Usuario";
            var vm = new AdministracionViewModel();
            vm.Usuarios = SingletonAuth.Instance.GetUsers();
            vm.Arbitros = SingletonAuth.Instance.GetArbitros();
            return View(vm);
        }

        [HttpPost]
        public async Task<string> RegistrarUsuario([FromBody] dynamic Request)
        {
            var usuario = (string)Request.Nombre;
            var psw = (string)Request.Password;
            var user = new IdentityUser { UserName = usuario, Email = usuario + "@sanisidro.com" };
            var result = await _userManager.CreateAsync(user, psw);
            if (result.Succeeded)
            {
                return JsonConvert.SerializeObject(new { result = true, message = $"El usuario {usuario} fue registrado exitosamente." });
            }
            else
            {
                return JsonConvert.SerializeObject(new { result = false, message = string.Join(" <br> ", result.Errors.Select(x => x.Description).ToArray()) });
            }
        }

        [HttpPost]
        public async Task<string> RegistrarArbitro([FromBody] dynamic Request)
        {
            var usuario = (string)Request.Nombre;
            var psw = (string)Request.Password;
            var user = new IdentityUser { UserName = usuario, Email = usuario + "@sanisidro.com" };
            var result = await _userManager.CreateAsync(user, psw);
            if (result.Succeeded)
            {
                var newUser = await _userManager.FindByNameAsync(user.UserName);
                await _userManager.AddToRoleAsync(newUser, "ARBITRO");

                return JsonConvert.SerializeObject(new { result = true, message = $"El usuario {usuario} fue registrado exitosamente." });
            }
            else
            {
                return JsonConvert.SerializeObject(new { result = false, message = string.Join(" <br> ", result.Errors.Select(x => x.Description).ToArray()) });
            }
        }

        [HttpGet]
        public async Task<string> GetModalRegistrar()
        {
            var html = await _viewRenderService.RenderToStringAsync2("", "~/Views/Administrador/Registrar.cshtml", null);

            return JsonConvert.SerializeObject(new { html });

        }

    }


}