using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using SAN_ISIDRO.Brigde;
using SAN_ISIDRO.Models.ViewModels;
using SAN_ISIDRO.Services;
using Newtonsoft.Json;
using System.Linq;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Logging;
using SAN_ISIDRO.Models;

namespace SAN_ISIDRO.Controllers
{
    [Authorize]
    public class ArbitrosController : Controller
    {
        Singleton<PartidoBridge> SingletonPartido;
        private readonly IViewRenderService _viewRenderService;


        public ArbitrosController(IViewRenderService viewRenderService)
        {
            _viewRenderService = viewRenderService;

            SingletonPartido = Singleton<PartidoBridge>.GetInstance();
        }

        [HttpGet]
        public IActionResult Index()
        {
            ViewData["Title"] = "Arbitros";
            return View();
        }

        [HttpGet]
        public IActionResult CedulaPartido(int IdPartido)
        {
            ViewData["Title"] = "Cedula de Partido";
            var vm = new CedulaPartido();
            vm = SingletonPartido.Instance.GetCedulaPartido(IdPartido);
            return View(vm);
        }

        [HttpPost]
        public async Task<string> GetPartidos()
        {
            var result = SingletonPartido.Instance.GetPartidosForArbitro(User.Identity.Name);
            var html = await _viewRenderService.RenderToStringAsync2("", "~/Views/Arbitros/PartidoMaker.cshtml", result);
            return JsonConvert.SerializeObject(new { html });
        }

        [HttpPost]
        public async Task<string> RegistrarEvidenciaPartido([FromBody] dynamic Request)
        {
            var IdPartido = (int)Request.IdPartido;
            var Img_Local = (string)Request.ImgLocal;
            var Img_Visita = (string)Request.ImgVisita;

            var result = await SingletonPartido.Instance.RegistrarEvidenciaPartido(User.Identity.Name, IdPartido, Img_Local, Img_Visita);
            return JsonConvert.SerializeObject(new { result = result == null ? true : false, message = result });
        }

        [HttpPost]
        public async Task<string> RegistrarAsistenciaEquipos([FromBody]dynamic Request)
        {
            var IdPartido = (int)Request.IdPartido;
            var Jugadores_Local = Request.JugadoresLocal.ToObject<List<int>>();
            var Jugadores_Visita = Request.JugadoresVisita.ToObject<List<int>>();

            var result = await SingletonPartido.Instance.RegistrarEvidenciaPartido(User.Identity.Name, IdPartido, Jugadores_Local, Jugadores_Visita);
            return JsonConvert.SerializeObject(new { result = result == null ? true : false, message = result });
        }
    }
}