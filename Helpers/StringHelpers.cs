using System.Globalization;
using System.Linq;
using System.Text;

namespace SAN_ISIDRO.Helpers
{
    public static class StringHelpers
    {
        public static string Normalized(string Value)
        {
            return Encoding.UTF8.GetString(Encoding.GetEncoding("UTF-8").GetBytes(Value)).ToUpper().Trim();
        }

        /*
            Metodo que Remueve Acentos en Valores necesarios
        */
        public static string RemoveDiacritics(string text)
        {
            if (string.IsNullOrWhiteSpace(text))
                return text;

            text = text.Normalize(NormalizationForm.FormD);
            var chars = text.Where(c => CharUnicodeInfo.GetUnicodeCategory(c) != UnicodeCategory.NonSpacingMark).ToArray();
            return new string(chars).Normalize(NormalizationForm.FormC);
        }
    }
}